<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LigneCommande extends Model
{
    use HasFactory;
    protected $tableName = "ligneCommandes";

    protected $fillable = [
        "qte","id_prouduit","id_commande"
    ];
}
